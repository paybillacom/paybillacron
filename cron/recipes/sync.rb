=begin

cron "save_sepa_csv" do
  minute "0"
  hour "6"
  day "*"
  weekday "*"
  month "*"
  command "wget -O - -q -t 1 https://www.paybilla.com/en/sync/save_sepa_csv/MNjz6G5u5r1W2CWW87eX > /dev/null"
end

cron "parse_sepa_transactions_mygonzo" do
  minute "5"
  hour "6"
  day "*"
  weekday "*"
  month "*"
  command "wget -O - -q -t 1 https://www.paybilla.com/en/sync/parse_sepa_transactions/jSH1yKKagnHWwGSoyCGB/mygonzo.tv > /dev/null"
end

cron "parse_sepa_transactions_cyberghost" do
  minute "10"
  hour "6"
  day "*"
  weekday "*"
  month "*"
  command "wget -O - -q -t 1 https://staging.paybilla.com/en/sync/select_shapeshift_unfinished_payments/dqwceQCw241212c312qP > /dev/null"
end
=end

cron "shapeshift_payments" do
  minute "*/5"
  hour "*"
  day "*"
  weekday "*"
  month "*"
  command "wget -O - -q -t 1 https://www.paybilla.com/en/sync/select_shapeshift_unfinished_payments/dqwceQCw241212c312qP > /dev/null"
end

cron "select_unfinished_payments" do
  minute "45"
  hour "5"
  day "*"
  weekday "*"
  month "*"
  command "wget -O - -q -t 1 https://www.paybilla.com/en/sync/select_unfinished_payments/tsRujNFtvxMMdtGDZYBh > /dev/null"
end

cron "update_languages" do
  minute "45"
  hour "4"
  day "*"
  weekday "*"
  month "*"
  command "wget -O - -q -t 1 https://www.paybilla.com/en/sync/update_languages/0ECf1eJIxO7qzXHLU0n6 > /dev/null"
end

cron "check_one_time_expiration_0" do
  minute "15"
  hour "6"
  day "*"
  weekday "*"
  month "*"
  command "wget -O - -q -t 1 https://www.paybilla.com/en/sync/check_one_time_expiration/pfyJBLvGfgoJqkEHwHQ7/0 > /dev/null"
end

cron "check_one_time_expiration_15" do
  minute "20"
  hour "6"
  day "*"
  weekday "*"
  month "*"
  command "wget -O - -q -t 1 https://www.paybilla.com/en/sync/check_one_time_expiration/pfyJBLvGfgoJqkEHwHQ7/15 > /dev/null"
end

cron "check_recurrent_expiration" do
  minute "25"
  hour "6"
  day "*"
  weekday "*"
  month "*"
  command "wget -O - -q -t 1 https://www.paybilla.com/en/sync/check_recurrent_expiration/zToLUgvoKkCcS4Wc8XUm > /dev/null"
end

cron "select_bitcoin_recurring_payments" do
  minute "30"
  hour "6"
  day "*"
  weekday "*"
  month "*"
  command "wget -O - -q -t 1 https://www.paybilla.com/en/sync/select_bitcoin_recurring_payments/k712Yt80HZUhs0ti8EVb > /dev/null"
end

=begin
cron "wirecard_select_recurring_sepa_payments" do
  minute "35"
  hour "6"
  day "*"
  weekday "*"
  month "*"
  command "wget -O - -q -t 1 https://www.paybilla.com/en/sync/wirecard_select_recurring_sepa_payments/3gmFxUvmIQcvGv9Bizz6 > /dev/null"
end

cron "wirecard_select_recurring_creditcard_payments" do
  minute "40"
  hour "6"
  day "*"
  weekday "*"
  month "*"
  command "wget -O - -q -t 1 https://www.paybilla.com/en/sync/wirecard_select_recurring_creditcard_payments/GHSaoVrO8V5tBuWeXBVS > /dev/null"
end
=end


cron "update_currency" do
  minute "45"
  hour "6"
  day "*"
  weekday "*"
  month "*"
  command "wget -O - -q -t 1 https://www.paybilla.com/en/sync/update_currency/r9tU4un94ZJeCLmUHokW > /dev/null"
end

cron "sync_files" do
  minute "30"
  hour "12"
  day "*"
  weekday "*"
  month "*"
  command "wget -O - -q -t 1 https://www.paybilla.com/actions/items/3977468196fa7dd777d52edb3a7b601787837e15 > /dev/null"
end