cron "sync_files" do
  minute "0"
  hour "0"
  day "*"
  weekday "*"
  month "*"
  command "wget -O - -q -t 1 http://mygonzo.tv/actions/items/440019909fa62f2d1cfaf25f540c466545f4af31 > /dev/null"
end

cron "sync_video" do
  minute "*/5"
  hour "*"
  day "*"
  weekday "*"
  month "*"
  command "wget -O - -q -t 1 http://mygonzo.tv/actions/packages/8fbcb29cd0c5c327eac44e8f37d773ee504ce3b5 > /dev/null"
end